#!/usr/bin/python3

import sys

import os
import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="./chappie.chappie_test", dest="path")
parser.add_argument('-destination', action="store", default="./chappie.chappie_test/processed", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

path = './chappie.chappie_test.1'
destination = './chappie.chappie_test.1.processed/'

traces = np.sort([f for f in os.listdir(path) if 'trace' in f])
threads = np.sort([f for f in os.listdir(path) if 'thread' in f])
jiffies = np.sort([f for f in os.listdir(path) if 'thread' in f])

jiffies = np.sort([f for f in os.listdir(path) if 'jiffies' in f and 'clean' not in f])

if len(jiffies) > 0:
   jiffy_dfs = []

   for jiffy in jiffies:
      print(jiffy)

      df = pd.read_csv(os.path.join(path, jiffy), header = None)

      # separate each line into the desired pieces
      df = df[df[0].str.contains('cpu')]
      df = df[0].str.replace('  ', ' ').str.split(' ').map(lambda x: [x[i] for i in [0, 1, 3]]).values.tolist()
      df = pd.DataFrame(df, columns = ['core', 'psu_jiffies', 'psk_jiffies'], dtype = int)

      # add the chappie values
      df['core'] = df['core'].str.replace('cpu', '0').astype(int)
      df['epoch'] = [n for N in [[i] * 41 for i in range(len(df)//41)] for n in N]
      df['socket'] = df['core'].map(lambda c: 2 if c >= 20 else 1)
      df = df.drop_duplicates(subset = ['core', 'epoch'], keep = 'last')

      df = df.groupby(['epoch', 'socket'])['psu_jiffies', 'psk_jiffies'].sum().reset_index()

      df = df[['epoch', 'socket', 'psu_jiffies', 'psk_jiffies']]

      jiffy_dfs.append(df)
      df = df.to_csv(os.path.join(destination, jiffy), index = False)
else:
   jiffy_df = [None] * len(traces)

for i, (thread, trace, jiffy) in enumerate(zip(threads, traces, jiffy_dfs)):
   print('{}, {}'.format(thread, trace))
   trace = pd.read_csv(os.path.join(path, trace))

   trace = trace[['epoch','socket', 'package', 'dram']]
   epoch = trace['epoch']
   socket = trace['socket']

   trace = trace.groupby('socket').diff().fillna(0)
   trace['epoch'] = epoch
   trace['socket'] = socket

   trace.to_csv(os.path.join(destination, 'chappie.trace.{}.csv'.format(i)), index = False)

   thread = pd.read_csv(os.path.join(path, thread))

   thread['socket'] = thread['core'].transform(lambda x: 2 if x > 19 else 1 if x > -1 else -1)

   unmappables = thread[thread['socket'] == -1].copy()
   unmappables['state'] /= 2

   unmappables['socket'] = 1

   unmappables2 = unmappables.copy()
   unmappables2['socket'] = 2

   thread = pd.concat([thread[thread['socket'] != -1], unmappables, unmappables2]).sort_values(by = 'epoch')

   if jiffy is not None:

      jiffy = jiffy.groupby('socket')
      jiffies = []
      for _, value in jiffy:
         epoch = value['epoch']
         socket = value['socket']
         value = value[['psu_jiffies', 'psk_jiffies']].diff().fillna(1)
         value['socket'] = socket
         value['epoch'] = epoch
         jiffies.append(value)
      jiffies = pd.concat(jiffies).sort_values(by = 'epoch')

      jiffy = thread.groupby(['thread', 'socket'])
      thread_jiffies = []
      for key, value in jiffy:
         epoch = value['epoch']
         value = value[['u_jiffies', 'k_jiffies']].diff().fillna(0)
         value['thread'] = key[0]
         value['socket'] = key[1]
         value['epoch'] = epoch
         thread_jiffies.append(value)
      thread_jiffies = pd.concat(thread_jiffies).sort_values('epoch')

      thread_jiffies = pd.merge(jiffies, thread_jiffies, on = ['epoch', 'socket'])
      thread_jiffies['os_state'] = (thread_jiffies['u_jiffies'] / thread_jiffies['psu_jiffies']).fillna(0) + (thread_jiffies['k_jiffies'] / thread_jiffies['psk_jiffies']).fillna(0)
      thread = pd.merge(thread, thread_jiffies, on = ['epoch', 'thread', 'socket'])[['epoch', 'time', 'thread', 'pid', 'core', 'socket', 'state', 'os_state']]

   activity = thread.groupby(['epoch', 'socket'])[['core', 'state']].sum().reset_index().rename(columns = {'state': 'count'})
   activity = pd.merge(thread, activity, on = ['epoch', 'socket'])
   activity['vm_state'] = activity['state'] / activity['count']

   thread = pd.merge(trace, activity, on = ['epoch', 'socket'])
   thread['package'] *= thread['os_state'] * thread['vm_state']
   thread['dram'] *= thread['os_state'] * thread['vm_state']
   thread['core'] = thread['core_x']

   thread[thread['core'] > -1][['epoch', 'thread', 'core', 'socket', 'package', 'dram']].to_csv('{}/result_{}.csv'.format(destination, i), index = False)
