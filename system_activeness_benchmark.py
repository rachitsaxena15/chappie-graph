import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "chappie.thread" in entry.name:
        numOfFiles += 1


for i in range(numOfFiles):
    with PdfPages('{}/system-activeness-{}_{}.pdf'.format(destination, benchmark, i)) as pdf:
        df = pd.read_csv(path+'chappie.thread.'+str(i)+'.csv')
        df = df.loc[df['thread'].isin(['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer'])]
        df = df[['epoch', 'thread', 'state']]
        df = df.loc[df['state'] == True]

        #Frequency 1
        bins = 10

        #Add binning info in column
        df['bin'] = df['epoch'].apply(lambda x: int(x/bins))

        #Group by bin and add sum frequency
        df = pd.DataFrame(df.groupby(['bin'], as_index=True)['thread'].value_counts())

        #rename new column name from thread to frequency
        df.columns = ['frequency']
        df.reset_index(inplace=True)
        df['bin_total'] = 0

        #new dataframe containing epoch and corresponding bin_total_count of threads
        newdf = pd.DataFrame(df.groupby(['bin'])['frequency'].sum())
        newdf.reset_index(inplace=True)
        newdf.columns = ['bin', 'bin_total']

        #set index and apply map on original dataframe
        s = newdf.set_index('bin')['bin_total']
        df['bin_total'] = df['bin'].map(s)

        #Calculate percentage of occurance
        df['percent'] = df['frequency']/df['bin_total']*100
        df = df.pivot(columns='thread', values='percent', index='bin')
        df = df.fillna(value=0)
        x = np.array(df.index)
        print(x)
        plotList = []
        labels = []
        for p in range(len(df.columns)):
            plotList.append(tuple(np.array(df.iloc[:, p])))
            labels.append(df.columns[p])

        print(plotList)
        print(labels)
        colors = ['skyblue', 'green', 'red', 'violet', 'orange']
        index = 0
        plt.figure(figsize=(70,50))
        for plotTuple in plotList:
            plt.plot(x, plotTuple, data=df, marker='o', markerfacecolor=colors[index], markersize=8,
                     color=colors[index], linewidth=1)
            index += 1

        plt.yticks(fontsize=30)
        plt.xticks(x, fontsize=30)
        plt.xlabel('Epoch: 1 unit as 10 epoch units', fontsize=40)
        plt.ylabel('Percent', fontsize=40)

        plt.legend(labels, prop={'size': 40})
        pdf.savefig()
        #plt.show()
        plt.close()
