import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)

benchmark = (list(benchmark.split(",")))
if not os.path.exists(destination):
    os.makedirs(destination)

with PdfPages(destination+'/system-application-attribution-all-bench.pdf') as pdf:
    firstPage = plt.figure(figsize=(30,20))
    firstPage.clf()
    txt = 'system-application-attribution-all-bench'
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=40, ha="center")
    pdf.savefig()
    plt.close()
    newDf = pd.DataFrame(columns=['benchmark', 'package_system', 'dram_system', 'package_sock1', 'package_sock2',
                                  'dram_sock1', 'dram_sock2'])
    count = 0
    for bench in benchmark:
        bench = bench.strip()
        print(bench)
        path = '{}/{}/processed_data/'.format(os.path.expanduser(args.path), bench)
        numOfFiles = 0
        for entry in os.scandir(path):
            if entry.is_file() and "system_application" in entry.name:
                numOfFiles += 1

        for i in range(numOfFiles):
            df = pd.read_csv(path+'/system_application_'+str(i)+'.csv')
            ind = df.index.max()
            package_system = df['sys_package'].sum()
            dram_system = df['sys_dram'].sum()

            package_sock1 = df['sock1_app_package'].sum()
            package_sock2 = df['sock2_app_package'].sum()
            dram_sock1 = df['sock1_app_dram'].sum()
            dram_sock2 = df['sock2_app_dram'].sum()
            newDf.loc[count] = [bench, package_system, dram_system, package_sock1, package_sock2, dram_sock1, dram_sock2]
            count+=1

    newDf = newDf.groupby(['benchmark'], as_index=False).sum()
    newDf.set_index('benchmark', inplace=True)
    print(newDf)
    plot_params = ['package_system', 'dram_system', 'package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2']
    newDf.plot.bar(stacked=True, figsize=(30, 20))
    plt.legend(loc='best')
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel('Benchmarks', fontsize=25)
    plt.ylabel('Total Energy (J)', fontsize=25)
    #plt.show()

    pdf.savefig()
    plt.close()
