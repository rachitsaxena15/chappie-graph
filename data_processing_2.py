import pandas as pd
import argparse
import os

#if mode == 1:  # 'OSNAIVE'
    # do stuff
#if mode == 2:  # OSSAMPLE
    # read_csv('khaled's csv)
    # read_csv('trace')
    # output it
    # do stuff
    # calculate using jiffies
#if mode == 3:  # VMSAMPLE
    # do stuff
    # read_csv('khaled's csv)
    # read_csv('trace')
    #  output it
    # calculate using activeness
    #everything is same

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="tomcat", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/chappie.dacapo/tomcat", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/processed_data/tomcat",
                    dest="destination")

args = parser.parse_args()
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
path = os.path.expanduser(args.path)
if not os.path.exists(destination):
   os.makedirs(destination)

print('Processing '+benchmark)
numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "chappie.thread" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    result = pd.read_csv(destination+'/result_'+str(i)+'.csv')
    newDf = pd.read_csv(destination+'/trace_'+str(i)+'.csv')
    thread = pd.read_csv(path+'/chappie.thread.'+str(i)+'.csv')

    #result = pd.read_csv('~/Desktop/result.csv')
    #newDf = pd.read_csv('~/Desktop/trace_0.csv')
    #thread = pd.read_csv('~/Desktop/raw_data/chappie.thread.0.csv')

    sys_app = pd.DataFrame(columns=['time', 'sys_package', 'sys_dram', 'sock1_app_package', 'sock1_app_dram',\
                                        'sock2_app_package', 'sock2_app_dram'])

    result = result.loc[result['core'] != -1]
    df_socket1 = result.loc[result['core'] < 20]
    df_socket2 = result.loc[result['core'] >= 20]

    dram_socket1 = df_socket1.groupby(['epoch'], as_index=False, sort=False)['dram'].sum()
    package_socket1 = df_socket1.groupby(['epoch'], as_index=False, sort=False)['package'].sum()

    dram_socket2 = df_socket2.groupby(['epoch'], as_index=False, sort=False)['dram'].sum()
    package_socket2 = df_socket2.groupby(['epoch'], as_index=False, sort=False)['package'].sum()
    times = result['epoch'].unique()
    count = 0

    for item in times:
        #T = result.iloc[item]['time']
        T = item
        system_threads = len(thread.loc[(thread['epoch'] == T) & (thread['core'] == -1)])

        total_package = newDf.loc[(newDf['epoch'] == T)]['package'].iloc[:].sum()
        total_dram = newDf.loc[(newDf['epoch'] == T)]['dram'].iloc[0].sum()

        total_active_bytes = thread.loc[(thread['epoch'] == T)]['bytes'].iloc[:].sum()
        system_bytes = thread.loc[(thread['epoch'] == T) & (thread['core'] == -1)]['bytes'].iloc[:].sum()
        system_dram = total_dram * (system_bytes / total_active_bytes)
        # new formula for system package
        system_package = total_package / system_threads

        try:
            sock1_app_package = package_socket1.loc[package_socket1['epoch'] == T]['package'].iloc[0]
        except Exception as e:
            sock1_app_package = 0
        try:
            sock2_app_package = package_socket2.loc[package_socket2['epoch'] == T]['package'].iloc[0]
        except Exception as e:
            sock2_app_package = 0
        try:
            sock1_app_dram = dram_socket1.loc[dram_socket1['epoch'] == T]['dram'].iloc[0]
        except Exception as e:
            sock1_app_dram = 0
        try:
            sock2_app_dram = dram_socket2.loc[dram_socket2['epoch'] == T]['dram'].iloc[0]
        except Exception as e:
            sock2_app_dram = 0

        sys_app.loc[count] = [T, system_package, system_dram, sock1_app_package, sock1_app_dram, sock2_app_package, \
                             sock2_app_dram]
        count += 1

    #sys_app.to_csv('~/Desktop/system-application.csv', index=False)
    sys_app.to_csv(destination+'/system-application_'+str(i)+'.csv', index=False)
