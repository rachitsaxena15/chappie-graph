import pandas as pd
import os

path =''
for i in range(1):
    df2 = pd.read_csv(path + str(i) + '_trace.csv')

    mode = int(os.environ['MODE'])

    # remove system threads
    #for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
    #    df = df[df['thread'] != choice]

    newDf = pd.DataFrame(columns=['thread', 'method', 'event', 'time', 'epoch', 'dram1', 'uncore1', 'package1',
                                   'dram2', 'uncore2', 'package2', 'jiffies'])

    if mode == 1: #'OSNAIVE'
        for iter in range(1, len(df)):
            df = pd.read_csv(path + str(i) + '_khaled.csv')
            result = pd.read_csv('result.csv')  #add path
            trace = pd.read_csv('result.csv')  #add path
            T = trace.iloc[iter]['time']
            # do stuff
            method = df.loc[iter]['method']
            event = df.loc[iter]['event']
            time = df.iloc[iter]['time']
            epoch = df.iloc[iter]['epoch']

            package1 = trace.loc[(trace['time'] == T) & (trace['socket'] == 1)]['package'].iloc[0]
            dram1 = trace.loc[(trace['time'] == T) & (trace['socket'] == 1)]['dram'].iloc[0]
            #uncore1 = ??

            dram2 = trace.loc[(trace['time'] == T) & (trace['socket'] == 2)]['dram'].iloc[0]
            package2 = trace.loc[(trace['time'] == T) & (trace['socket'] == 2)]['package'].iloc[0]
            #uncore2 = ???

            '''dram1 = df.iloc[index]['dram1'] - df.iloc[index-1]['dram1']
            uncore1 = df.iloc[index]['uncore1'] - df.iloc[index-1]['uncore1']
            package1 = df.iloc[index]['package1'] - df.iloc[index-1]['package1']

            dram2 = df.iloc[index]['dram2'] - df.iloc[index - 1]['dram2']
            uncore2 = df.iloc[index]['uncore2'] - df.iloc[index - 1]['uncore2']
            package2 = df.iloc[index]['package2'] - df.iloc[index - 1]['package2']'''


            if dram1 < 0:
                dram1 += 16384
            if package1 < 0:
                package1 += 16384
            if dram2 < 0:
                dram2 += 16384
            if package2 < 0:
                package2 += 16384

            #attribution

            #output new data frame
            for index in df['methods'].unique():  # methods may appear more than once - change unique() to ---

                newDf = pd.DataFrame('khaled csv')

                method = newDf.iloc[index]['method']  # get method name at index
                start_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'on')]['time'][0]  # first start
                end_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'off')]['time'][0]  # first end

                # calculate energies for a method between start and end timestamp and add them together
                #check for method start and end timestamp and add energies from trace
                total_dram1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['dram1'].iloc[:].sum()
                total_package1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['package1'].iloc[
                                 :].sum()
                total_uncore1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['uncore1'].iloc[
                                :].sum()

                total_dram2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['dram2'].iloc[:].sum()
                total_package2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['package2'].iloc[
                                 :].sum()
                total_uncore2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['uncore2'].iloc[
                                :].sum()

                # number of methods appearing between start and end time
                total_methods = len((newDf.loc[((newDf['time'] <= end_time) & newDf['time'] >= start_time)]).loc[newDf['method']])

                # Energy of method in hand will be the average of total energies appearing within timestamps per method
                dram1 = total_dram1 / total_methods
                package1 = total_package1 / total_methods
                uncore1 = total_uncore1 / total_methods

                dram2 = total_dram2 / total_methods
                package2 = total_package1 / total_methods
                uncore2 = total_uncore1 / total_methods

    if mode == 2: #OSSAMPLE
        newDf = pd.read_csv(path + str(i) + '_khaled.csv')
        result = pd.read_csv('result.csv')  # add path
        trace = pd.read_csv('trace_0.csv')
        # read_csv('khaled's csv)
        # read_csv('trace')
        # output it
        # do stuff
        # calculate using jiffies   --data awaited. further actions based on jiffy data
        newDf = newDf.groupby(['method', 'time'])  # sort time in asc if not already

        p = 0
        for index in df['methods'].unique():  # methods may appear more than once - change unique() to ---

            ### read energies from result.csv, not khaled's file###
            package1 = trace.loc[(trace['time'] == T) & (trace['socket'] == 1)]['package'].iloc[0]
            dram1 = trace.loc[(trace['time'] == T) & (trace['socket'] == 1)]['dram'].iloc[0]
            # uncore1 = ??

            dram2 = trace.loc[(trace['time'] == T) & (trace['socket'] == 2)]['dram'].iloc[0]
            package2 = trace.loc[(trace['time'] == T) & (trace['socket'] == 2)]['package'].iloc[0]
            # uncore2 = ???

            method = newDf.iloc[index]['method']  # get method name at index
            start_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'on')]['time'][0]  # first start
            end_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'off')]['time'][0]  # first end

            # number of methods appearing between start and end time
            total_methods = len((newDf.loc[((newDf['time'] <= end_time) & newDf['time'] >= start_time)]).loc[newDf['method']])

            # Energy of method in hand will be the average of total energies appearing within timestamps per method
            # consider 20 methods of same name, group by - add energy - average out by 20 #####
            dram1 = total_dram1 / total_methods
            package1 = total_package1 / total_methods
            uncore1 = total_uncore1 / total_methods

            dram2 = total_dram2 / total_methods
            package2 = total_package1 / total_methods
            uncore2 = total_uncore1 / total_methods

            result.loc[p] = [method, dram1, package1, uncore1, dram2, package2, uncore2]

    if mode == 3: #VMSAMPLE (or mode 2)
        # do stuff
        # read_csv('khaled's csv)
        # read_csv('result.csv')
        # read('trace.csv')
        #  output it
        # calculate using activeness ###    -- need to sync b/w thread and trace_0 csvs
        newDf = pd.read_csv(path + str(i) + '_khaled.csv')
        result = pd.read_csv('result.csv')  # add path
        trace = pd.read_csv('trace_0.csv')
        newDf = newDf.groupby(['method', 'time'])  # sort time in asc if not already

        # if mode ==2 || mode ==3:
        for index in range(1, len(df)):
            # read_csv('result.csv')
            # read('trace.csv')

            # thread = df.loc[index]['thread']
            # do not read energies from khaled's file, read from result.csv

            # newDf.to_csv('~/Desktop/method_attrib_'+str(i)+'.csv', index=False)

            # result = pd.DataFrame(columns=['method', 'dram1', 'package1', 'uncore1', 'dram2', 'package2', 'uncore2'])
            df = df.groupby(['method', 'time'])       # sort time in asc if not already

            p = 0
            for index in df['methods'].unique():    #methods may appear more than once - change unique() to ---

                ### read energies from result.csv, not khaled's file###
                # consider 20 methods of same name, group by - add energy - average out by 20
                method = newDf.iloc[index]['method']    # get method name at index
                start_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'on')]['time'][0]   #first start
                end_time = newDf.loc[(newDf['method'] == method) & (newDf['event'] == 'off')]['time'][0]    #first end

                # calculate energies for a method between start and end timestamp and add them together
                total_dram1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['dram1'].iloc[:].sum()
                total_package1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['package1'].iloc[:].sum()
                total_uncore1 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['uncore1'].iloc[:].sum()

                total_dram2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['dram2'].iloc[:].sum()
                total_package2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['package2'].iloc[:].sum()
                total_uncore2 = newDf.loc[(newDf['method'] == method) & (newDf['time'] < end_time)]['uncore2'].iloc[:].sum()

                # number of methods appearing between start and end time
                total_methods = len((newDf.loc[((newDf['time'] <= end_time) & newDf['time'] >= start_time)]).loc[newDf['method']])

                #Energy of method in hand will be the average of total energies appearing within timestamps per method
                dram1 = total_dram1/total_methods
                package1 = total_package1/total_methods
                uncore1 = total_uncore1/total_methods

                dram2 = total_dram2/total_methods
                package2 = total_package1/total_methods
                uncore2 = total_uncore1/total_methods

                result.loc[p] = [method, dram1, package1, uncore1, dram2, package2, uncore2]

                #concept of method map ?
                #need more clarity in data
                #

    print(len(thread))