import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow/processed_data", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data/graphs",
                    dest="destination")
parser.add_argument('-kind', action="store", default="2CFA", dest="kind")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
kind = args.kind
destination = os.path.expanduser(args.destination)

fileName = ''

if not os.path.exists(destination):
    os.makedirs(destination)
bigData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
total_energy = 0

if kind in '1CFA':
    fileName = '1CFA_attribution_'
elif kind in '2CFA':
    fileName = '2CFA_attribution_'
elif kind in 'method':
    fileName = 'method_attribution_'
elif kind in 'java':
    fileName = 'javathread_attribution_'
print(kind)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and fileName in entry.name:
        print(entry.name)
        numOfFiles += 1
for i in range(3, numOfFiles):
    df = pd.read_csv('{}/{}{}.csv'.format(path, fileName, i))
    #df = pd.read_csv(path + '/method_attribution_' + str(i) + '.csv')
    bigData = bigData.append(df, ignore_index=True)
    bigData = bigData.fillna(value=0)
    data = pd.read_csv(path + '/result_' + str(i) + '.csv')
    data['energy'] = data['dram'] + data['package']
    total_energy += data['energy'].sum()


bigData = bigData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
    .sum(numeric_only=False)
bigData.reset_index(inplace=True)
with PdfPages(destination + '/method-energy-proportion-' + benchmark + '.pdf') as pdf:
    # firstPage = plt.figure(figsize=(15,10))
    # firstPage.clf()
    # txt = 'method-energy-proportion-' + benchmark
    # firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=24, ha="center")
    # pdf.savefig()
    # plt.close()
    newDf = pd.DataFrame(columns=['method', 'percent'])
    count = 0
    for method in bigData['method']:
        energy = float(bigData.loc[bigData['method'] == method]['energy'])
        percent = energy / total_energy * 100
        newDf.loc[count] = [method, percent]
        count += 1

    # remove Thread.getStackTrace
    newDf = newDf.loc[newDf['method'] != 'Thread.getStackTrace']
    newDf = newDf.sort_values('percent', ascending=False).head(10)
    print(newDf)

    mpl.rcParams.update({'font.size': 8})

    x = np.array(newDf['method'])
    ax = newDf.plot(kind='barh', x='method', y=['percent'], width=0.2, figsize=(6, 4), color='red')

    ax.set_yticklabels([])
    ax.invert_yaxis()
    ax.tick_params(
        axis='y',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False)  # labels along the bottom edge are off

    for i, v in enumerate(x):
        ax.text(0, i - .20, str(v), color='black', fontweight='bold')

    plt.xlabel('Percent Consumption of Energy')
    plt.tight_layout()
    pdf.savefig()
    #plt.show()
    plt.close()
