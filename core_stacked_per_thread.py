import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import re

path = '~/Desktop/graphchi_logs/'
benchmark = 'graphchi'
with PdfPages(benchmark + '_core_stacked.pdf') as pdf:
    for i in range(10):
        df = pd.read_csv(path + 'chappie.thread.' + str(i) + '.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]

        df = df.loc[df['core'] != -1]

        df['energy'] = df['package'] + df['dram']
        df = df.groupby(['thread', 'core'], as_index=False, sort=False)['energy'].sum()
        print(df)

        df_pivot = df.pivot(index='thread', columns='core', values='energy')
        df_pivot = df_pivot.fillna(0)

        print(list(df_pivot.columns.values))

        if df_pivot.empty:
            continue

        color_pallet = ['#e6194b', '#3cb44b', '#ffe119', '#0082c8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', \
                        '#d2f53c', '#fabebe', '#008080', '#e6beff', '#aa6e28', '#fffac8', '#800000', '#aaffc3', \
                        '#808000', '#ffd8b1', '#000080', '#808080', '#FFFFFF', '#000000']

        df_pivot['sum'] = df_pivot.sum(axis=1)
        data1 = df_pivot.loc[:, range(20)]
        data2 = df_pivot.loc[:, range(20, 40)]

        print(data1)

        pd.set_option("display.max_columns", 40)
        # pd.set_option('display.max_colwidth', -1)

        max1 = max((df_pivot.loc[:, range(20)]).sum(axis=1))
        max2 = max((df_pivot.loc[:, range(20, 40)]).sum(axis=1))
        print(max1, max2)


        fig, ax = plt.subplots(2, 1)

        ax = ax.ravel()
        fig = plt.figure()

        # core 1
        ax[0] = fig.add_subplot(2, 1, 1)
        data1.plot.bar(stacked=True, color=color_pallet, figsize=(15, 20), ax=ax[0])
        # ax[0].set_aspect('equal')
        plt.legend(loc='best', bbox_to_anchor=(1.10, 1), borderaxespad=0, prop={'size': 15})
        ax[0].xaxis.set_tick_params(labelsize=15)
        ax[0].yaxis.set_tick_params(labelsize=15)
        plt.xlabel('Threads (Socket 1)', fontsize=15)
        plt.xticks(rotation=80)
        plt.ylabel('Energy (J)', fontsize=15)
        # plt.yticks(np.arange(0, data1['sum'].max(), .5))
        plt.yticks(np.arange(0, math.ceil(max1)))

        # core 2
        ax[1] = fig.add_subplot(2, 1, 2)
        data2.plot.bar(stacked=True, color=color_pallet, figsize=(15, 20), ax=ax[1])
        # ax[1].set_aspect('equal')
        plt.legend(loc='best', bbox_to_anchor=(1.10, 1), borderaxespad=0, prop={'size': 15})
        ax[1].xaxis.set_tick_params(labelsize=15)
        ax[1].yaxis.set_tick_params(labelsize=15)
        plt.xlabel('Threads (Socket 2)', fontsize=15)
        plt.xticks(rotation=80)
        plt.ylabel('Energy (J)', fontsize=15)
        plt.yticks(np.arange(0, math.ceil(max2)))

        plt.suptitle('PER THREAD ENERGY CONSUMPTION FROM chappie.thread.' + str(i) + '.csv', fontsize=10)

        # Add a horizontal line
        # plt.plot([-1, 1.5], [1.01, 1.01], color='gray', lw=1, transform=ax[1].transAxes, clip_on=False)

        plt.tight_layout()
        pdf.savefig()
        # plt.show()
        plt.close()
