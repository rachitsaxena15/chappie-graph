import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages

benchmark = 'graphchi1'
path = '~/Desktop/graphchi_logs/'
with PdfPages(benchmark+'-thread-vs-energy.pdf') as pdf:
    for i in range(10):
        df = pd.read_csv(path+'chappie.thread.'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]

        df['total_energy'] = df['package'] + df['dram']

        print(i)

        #socket1
        data_socket1 = df[df.core < 20]

        #socket2
        data_socket2 = df[df.core >= 20]

        fig, ax = plt.subplots(1, 2, figsize=(15, 20))
        ax = ax.ravel()
        fig = plt.figure(figsize=(15, 20))
        iter =0
        count =1
        for data in [data_socket1, data_socket2]:
            #fig = plt.figure(figsize=(15,10))
            a = data['core']
            b = data['package']
            c = data['dram']
            d = data['bytes']

            fig.suptitle('Thread Vs Energy for Core 0 and Core 1 - chappie.thread.' + str(i) + '.csv')

            ax[(iter)] = fig.add_subplot(2, 1, (iter) + 1)
            sns.set(style="whitegrid")
            sns.barplot(x='thread', y='total_energy', data = data, ci=None)
            #plt.legend(loc='best')
            plt.ylabel('Total Energy (DRAM+Package) - Core '+ str(count))
            plt.xticks(rotation=90)

            iter += 1
            count += 1

        pdf.savefig()
        plt.close()

